import fiona
import rasterio
import rasterio.mask
from rasterio.plot import show
from rasterio.warp import calculate_default_transform, reproject, Resampling

import geopandas as gpd


# Read Shape file
with fiona.open(
    "/home/hubert/Downloads/SRVGridsAndPoints/AllGrids_SRV_2022.shp", "r"
) as shapefile:
    shapes = [feature["geometry"] for feature in shapefile]

# read imagery file
with rasterio.open("/home/hubert/Downloads/rgb_2021-01-04.tif") as src:
    out_image, out_transform = rasterio.mask.mask(shapes, src, crop=True)
    out_meta = src.meta

# Save clipped imagery
out_meta.update(
    {
        "driver": "GTiff",
        "height": out_image.shape[1],
        "width": out_image.shape[2],
        "transform": out_transform,
    }
)

with rasterio.open(
    "/home/hubert/Downloads/imagery_trans_clip.tif", "w", **out_meta
) as dest:
    dest.write(out_image)
