import cv2
import os
from skimage import io
import skimage.io as io
from skimage.color import rgb2gray
import rasterio


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT = BASE_DIR + "/"
os.chdir(PROJECT_ROOT)


def init_array(rgb_array):
    working_array = rgb_array.copy()
    for i in range(0, rgb_array.shape[0]):
        for j in range(0, rgb_array.shape[1]):
            if rgb_array[i, j] > 0:
                working_array[i, j] = 1
            else:
                working_array[i, j] = 0
    return working_array


# Get the project root directory
project_path = PROJECT_ROOT
RCNN_ROOT = os.path.abspath(project_path)
os.chdir(RCNN_ROOT)
path_to_image = PROJECT_ROOT + "2022-matam/2022/rgb/rgb_2022-01-04.tif"
img = cv2.imread(path_to_image)  # GRAY
img = rgb2gray(img)
io.imsave(
    os.path.join(PROJECT_ROOT + "2022-matam/output/test.tif",), init_array(img),
)

img = rasterio.open(PROJECT_ROOT + "2022-matam/output/test.tif").read(1)
profile = rasterio.open(PROJECT_ROOT + "2022-matam/output/test.tif", driver="Gtiff").profile
with rasterio.open(
    PROJECT_ROOT + "2022-matam/output/matam{}".format(".tif"),
    "w",
    **profile,
) as dst:
    dst.write(init_array(img).astype(rasterio.uint8), 1)