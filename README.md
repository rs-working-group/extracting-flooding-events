# Extracting Flooding Events

What we are trying to do here is to extract on a grid-cell level the flooded areas, flooding dates and proportions using MNDWI index, RGB images and CLOUD probability from Sentinel-2 imagery.

# Getting Started

The workflow is divided into 3 steps: 1. Downloading images from Google Earth Engine, 2. Pre-process the downloaded images to select images to work with and then generate the flooding maps raster for the AOI across a given growing season, and lastly 3. extract flooded areas and their corresponding proportions.

* [downalod_images.py](downalod_images.py): Is the easiest way to start. It shows how to download high resolution images fron Sentinel-2

Case study:

1. Matam: The initial code were modified as for Matam two orbits of Sentinel 2 are needed to cover the whole AOI. So we had to set up a 3 days time range to mosaic all images falling in this interval.

Note that for Dagana and Podor, we are only mosaicking the images acquired on the same day to get a complete coverage of our AOI.

2. For Dagana and Podor only one orbit is needed

* [processing_images.py](processing_imaages.py): shows how to mosaic downloaded images and process them.

* [extract_data.py](extract_data.py): This file contains the main of getting flooding dates and areas. 



