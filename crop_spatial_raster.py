"""
@author: Hubert K

"""
# ## Georeference Masks
# Example found in the workstation workflow
# The aim of this script is to convert prediction patches saved in jp, png, etc to TIF format referencing the metadata of the original satelite imagery
# https://rasterio.readthedocs.io/en/latest/topics/masks.html
#
import os
import rasterio as rio
import skimage.io as io
from datetime import datetime
import pathlib
from osgeo import gdal
import ogr, gdal, osr
from flusstools import geotools

# import required libraries
import os
import rasterio
from rasterstats import zonal_stats
from datetime import datetime

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT = BASE_DIR + "/"
os.chdir(PROJECT_ROOT)


import fiona
import rasterio
import rasterio.mask

with fiona.open("SRV/SRVGridsAndPoints/AllGrids_SRV_2022.shp", "r") as shapefile:
    shapes = [feature["geometry"] for feature in shapefile]


with rasterio.open("SRV/SRV_MiddleValley_Podor.kml") as src:
    out_image, out_transform = rasterio.mask.mask(src, shapes, crop=True)
    out_meta = src.meta


out_meta.update(
    {
        "driver": "GTiff",
        "height": out_image.shape[1],
        "width": out_image.shape[2],
        "transform": out_transform,
    }
)

with rasterio.open("SRV_podor.tif", "w", **out_meta) as dest:
    dest.write(out_image)
